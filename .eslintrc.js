module.exports = {
  "root": true,
  "parserOptions": {
    "parser": "babel-eslint",
    "sourceType": "module"
  },
  "extends": [
    'plugin:nuxt/recommended',
    'plugin:vue/recommended'
  ],
  rules: {
    "vue/max-attributes-per-line": [
      2,
      {
        "singleline": 1,
        "multiline": {
          "max": 1,
          "allowFirstLine": true
        }
      }
    ],
    "vue/html-closing-bracket-newline": ["error", {
      "singleline": "never",
      "multiline": "never"
    }]
  }
}